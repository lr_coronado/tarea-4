﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TAREA4
{
    public partial class frmRegister : Form
    {
        public frmRegister()
        {
            InitializeComponent();
        }

        private void frmRegister_Load(object sender, EventArgs e)
        {

        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            try
            {
                if (mtValidarInput())
                {
                    Program.RequestCreator.initCreator("SPC_USERS_PUT", RequestType.UpdateData);
                    Program.RequestCreator.ClearParams();
                    Program.RequestCreator.AddParameter("@NOMBRE_U", boxName.Text.Trim());
                    Program.RequestCreator.AddParameter("@APELLIDO_U", boxLN.Text.Trim());
                    Program.RequestCreator.AddParameter("@MAIL_U", boxMail.Text.Trim());
                    Program.RequestCreator.AddParameter("@TELEFONO_U", boxTelefono.Text.Trim());
                    Program.RequestCreator.AddParameter("@USERNAME_U", boxUserName.Text.Trim());
                    Program.RequestCreator.AddParameter("@PASSWORD_U", boxPassword.Text.Trim());
                    Program.RequestCreator.AddParameter("@OPT", 0);
                    Program.RequestCreator.Create();

                    MessageBox.Show("Se agregó el usuario correctamente.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private bool mtValidarInput() 
        {
            if (boxName.Text != string.Empty)
            {
                if (boxLN.Text != string.Empty)
                {
                    if (boxMail.Text != string.Empty)
                    {
                        if (boxTelefono.Text != string.Empty)
                        {
                            if (boxUserName.Text != string.Empty)
                            {
                                if (boxPassword.Text != string.Empty)
                                {
                                    return true;
                                }
                                else
                                {
                                    MessageBox.Show("El campo de Password o Contraseña está vacío.", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                    return false;
                                }
                            }
                            else
                            {
                                MessageBox.Show("El campo de User Name está vacío.", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                return false;
                            }
                        }
                        else
                        {
                            MessageBox.Show("El campo de Teléfono está vacío.", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            return false;
                        }
                    }
                    else
                    {
                        MessageBox.Show("El campo de Email está vacío.", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return false;
                    }
                }
                else
                {
                    MessageBox.Show("El campo de Apellido está vacío.", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return false;
                }
            }
            else
            {
                MessageBox.Show("El campo de Nombre está vacío.", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
        }

        private void btnSalirLogin_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
