﻿
namespace TAREA4
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgMain = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.btnSalirLogin = new System.Windows.Forms.Button();
            this.dgPanel = new System.Windows.Forms.Panel();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NOMBRE_U = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.APELLIDO_U = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MAIL_U = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TELEFONO_U = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.USERNAME_U = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PASSWORD_U = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FECHA_INSERT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EditarBtn = new System.Windows.Forms.DataGridViewButtonColumn();
            this.DeleteBtn = new System.Windows.Forms.DataGridViewButtonColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgMain)).BeginInit();
            this.dgPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgMain
            // 
            this.dgMain.AllowUserToAddRows = false;
            this.dgMain.AllowUserToDeleteRows = false;
            this.dgMain.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgMain.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgMain.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.NOMBRE_U,
            this.APELLIDO_U,
            this.MAIL_U,
            this.TELEFONO_U,
            this.USERNAME_U,
            this.PASSWORD_U,
            this.FECHA_INSERT,
            this.EditarBtn,
            this.DeleteBtn});
            this.dgMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgMain.Location = new System.Drawing.Point(0, 0);
            this.dgMain.Name = "dgMain";
            this.dgMain.ReadOnly = true;
            this.dgMain.Size = new System.Drawing.Size(769, 324);
            this.dgMain.TabIndex = 0;
            this.dgMain.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgMain_CellContentClick);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 19F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(125)))), ((int)(((byte)(193)))));
            this.label1.Location = new System.Drawing.Point(340, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(123, 30);
            this.label1.TabIndex = 1;
            this.label1.Text = "Usuarios";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(125)))), ((int)(((byte)(193)))));
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button2.Location = new System.Drawing.Point(26, 400);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(158, 36);
            this.button2.TabIndex = 2;
            this.button2.Text = "NUEVO";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnSalirLogin
            // 
            this.btnSalirLogin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSalirLogin.BackColor = System.Drawing.Color.DarkRed;
            this.btnSalirLogin.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSalirLogin.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnSalirLogin.Location = new System.Drawing.Point(663, 400);
            this.btnSalirLogin.Name = "btnSalirLogin";
            this.btnSalirLogin.Size = new System.Drawing.Size(132, 36);
            this.btnSalirLogin.TabIndex = 3;
            this.btnSalirLogin.Text = "CERRAR";
            this.btnSalirLogin.UseVisualStyleBackColor = false;
            this.btnSalirLogin.Click += new System.EventHandler(this.btnSalirLogin_Click);
            // 
            // dgPanel
            // 
            this.dgPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgPanel.Controls.Add(this.dgMain);
            this.dgPanel.Location = new System.Drawing.Point(26, 70);
            this.dgPanel.Name = "dgPanel";
            this.dgPanel.Size = new System.Drawing.Size(769, 324);
            this.dgPanel.TabIndex = 4;
            // 
            // ID
            // 
            this.ID.DataPropertyName = "ID_U";
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.ReadOnly = true;
            // 
            // NOMBRE_U
            // 
            this.NOMBRE_U.DataPropertyName = "NOMBRE_U";
            this.NOMBRE_U.HeaderText = "Nombre";
            this.NOMBRE_U.Name = "NOMBRE_U";
            this.NOMBRE_U.ReadOnly = true;
            // 
            // APELLIDO_U
            // 
            this.APELLIDO_U.DataPropertyName = "APELLIDO_U";
            this.APELLIDO_U.HeaderText = "Apellido";
            this.APELLIDO_U.Name = "APELLIDO_U";
            this.APELLIDO_U.ReadOnly = true;
            // 
            // MAIL_U
            // 
            this.MAIL_U.DataPropertyName = "MAIL_U";
            this.MAIL_U.HeaderText = "Mail";
            this.MAIL_U.Name = "MAIL_U";
            this.MAIL_U.ReadOnly = true;
            // 
            // TELEFONO_U
            // 
            this.TELEFONO_U.DataPropertyName = "TELEFONO_U";
            this.TELEFONO_U.HeaderText = "Telefono";
            this.TELEFONO_U.Name = "TELEFONO_U";
            this.TELEFONO_U.ReadOnly = true;
            // 
            // USERNAME_U
            // 
            this.USERNAME_U.DataPropertyName = "USERNAME_U";
            this.USERNAME_U.HeaderText = "UserName";
            this.USERNAME_U.Name = "USERNAME_U";
            this.USERNAME_U.ReadOnly = true;
            // 
            // PASSWORD_U
            // 
            this.PASSWORD_U.DataPropertyName = "PASSWORD_U";
            this.PASSWORD_U.HeaderText = "Password";
            this.PASSWORD_U.Name = "PASSWORD_U";
            this.PASSWORD_U.ReadOnly = true;
            // 
            // FECHA_INSERT
            // 
            this.FECHA_INSERT.DataPropertyName = "FECHA_INSERT";
            this.FECHA_INSERT.HeaderText = "Fecha";
            this.FECHA_INSERT.Name = "FECHA_INSERT";
            this.FECHA_INSERT.ReadOnly = true;
            // 
            // EditarBtn
            // 
            this.EditarBtn.HeaderText = "Editar";
            this.EditarBtn.Name = "EditarBtn";
            this.EditarBtn.ReadOnly = true;
            this.EditarBtn.Text = "Editar";
            this.EditarBtn.UseColumnTextForButtonValue = true;
            // 
            // DeleteBtn
            // 
            this.DeleteBtn.HeaderText = "Eliminar";
            this.DeleteBtn.Name = "DeleteBtn";
            this.DeleteBtn.ReadOnly = true;
            this.DeleteBtn.Text = "Eliminar";
            this.DeleteBtn.UseColumnTextForButtonValue = true;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(817, 455);
            this.Controls.Add(this.dgPanel);
            this.Controls.Add(this.btnSalirLogin);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Main";
            this.TopMost = true;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgMain)).EndInit();
            this.dgPanel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgMain;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button btnSalirLogin;
        private System.Windows.Forms.Panel dgPanel;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn NOMBRE_U;
        private System.Windows.Forms.DataGridViewTextBoxColumn APELLIDO_U;
        private System.Windows.Forms.DataGridViewTextBoxColumn MAIL_U;
        private System.Windows.Forms.DataGridViewTextBoxColumn TELEFONO_U;
        private System.Windows.Forms.DataGridViewTextBoxColumn USERNAME_U;
        private System.Windows.Forms.DataGridViewTextBoxColumn PASSWORD_U;
        private System.Windows.Forms.DataGridViewTextBoxColumn FECHA_INSERT;
        private System.Windows.Forms.DataGridViewButtonColumn EditarBtn;
        private System.Windows.Forms.DataGridViewButtonColumn DeleteBtn;
    }
}

