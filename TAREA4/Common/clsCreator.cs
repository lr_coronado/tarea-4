﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAREA4.Common;
using TAREA4.Entidades;
using TAREA4.Request;

namespace TAREA4
{
    public abstract class clsCreator
    {

    }

    public class RequestCreator : clsCreator
    {
        string CommandRequested;
        string ConnectionString;
        public RequestType RequestType;
        List<KeyValuePair<string, object>> Parametros;

        // Constructor 
        public RequestCreator()
        {
            this.ConnectionString = Program.Connection.constructString();
            Parametros = new List<KeyValuePair<string, object>>();
        }

        /*
         *  Inicializador del Creador de requests
         */
        public void initCreator(string command, RequestType request)
        {
            ClearParams();
            this.CommandRequested = command;
            this.RequestType = request;
        }

        /*
         * Metodo que se utiliza para crear los requests
         */
        public object Create()
        {
            switch (this.RequestType)
            {
                case RequestType.DataGrid: return mtGetDataTable();
                case RequestType.SingleData: return mtGetStringData();
                case RequestType.SingleLoginData: return mtGetStringLoginData();
                case RequestType.InsertData: return mtExcuteCommand();
                case RequestType.DeleteData: return mtExcuteCommand();
                case RequestType.UpdateData: return mtExcuteCommand();
                default: return 0;
            }
        }

        /*
         * Metodos axuliares para funcionalidades de parametros
         */
        public void ClearParams()
        {
            this.Parametros.Clear();
        }
        public void AddParameter(string name, object value)
        {
            this.Parametros.Add(new KeyValuePair<string, object>(name, value));
        }

        /*
         *  Metodos para la obtencion de la data
         */
        DataTable mtGetDataTable()
        {
            DataTableGrid n = new DataTableGrid(this.ConnectionString, this.CommandRequested, this.Parametros);
            return n.Datos;
        }

        string mtGetStringData()
        {
            SingleData n = new SingleData(this.ConnectionString, this.CommandRequested, this.Parametros);
            return n.Datos;
        }
        string mtGetStringLoginData()
        {
            SingleLoginData n = new SingleLoginData(this.ConnectionString, this.CommandRequested, this.Parametros);
            return n.Datos;
        }

        bool mtExcuteCommand()
        {
            CommandExecuteNonQuery n = new CommandExecuteNonQuery(this.ConnectionString, this.CommandRequested, this.Parametros);
            if (n != null)
                return true;
            else
                return false;
        }

        /*
         * Destructor o desconstructor
         */
        ~RequestCreator()
        {
            GC.Collect();
        }
    }
}
