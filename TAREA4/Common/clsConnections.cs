﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace TAREA4.Common
{
    public sealed class clsConnections
    {
        string DB_HOST { get; set; }
        string DB_NAME { get; set; }
        string DB_USER { get; set; }
        string DB_PASS { get; set; }

        public clsConnections() 
        {
            this.DB_HOST = ConfigurationManager.AppSettings["DB_HOST"]?.ToString().Trim();
            this.DB_NAME = ConfigurationManager.AppSettings["DB_NAME"]?.ToString().Trim();
            this.DB_USER = ConfigurationManager.AppSettings["DB_USER"]?.ToString().Trim();
            this.DB_PASS = ConfigurationManager.AppSettings["DB_PASS"]?.ToString().Trim();
        }

        public string constructString() 
        {
            return string.Format("Data Source={0};Initial Catalog={1};User Id={2};Password={3};Integrated Security=True;", DB_HOST, DB_NAME, DB_USER, DB_PASS);
        }
    }
}
