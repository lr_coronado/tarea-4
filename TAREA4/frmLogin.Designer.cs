﻿
namespace TAREA4
{
    partial class frmLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.btnSalirLogin = new System.Windows.Forms.Button();
            this.mtxtPass = new System.Windows.Forms.MaskedTextBox();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.btnIngLogin = new System.Windows.Forms.Button();
            this.btnRegister = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 19F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(125)))), ((int)(((byte)(193)))));
            this.label1.Location = new System.Drawing.Point(136, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 30);
            this.label1.TabIndex = 0;
            this.label1.Text = "LOGIN ";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // btnSalirLogin
            // 
            this.btnSalirLogin.BackColor = System.Drawing.Color.DarkRed;
            this.btnSalirLogin.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSalirLogin.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnSalirLogin.Location = new System.Drawing.Point(25, 183);
            this.btnSalirLogin.Name = "btnSalirLogin";
            this.btnSalirLogin.Size = new System.Drawing.Size(142, 36);
            this.btnSalirLogin.TabIndex = 1;
            this.btnSalirLogin.Text = "SALIR";
            this.btnSalirLogin.UseVisualStyleBackColor = false;
            this.btnSalirLogin.Click += new System.EventHandler(this.btnSalirLogin_Click);
            // 
            // mtxtPass
            // 
            this.mtxtPass.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.mtxtPass.Location = new System.Drawing.Point(26, 132);
            this.mtxtPass.Name = "mtxtPass";
            this.mtxtPass.PasswordChar = '*';
            this.mtxtPass.Size = new System.Drawing.Size(319, 21);
            this.mtxtPass.TabIndex = 2;
            // 
            // txtUserName
            // 
            this.txtUserName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.txtUserName.Location = new System.Drawing.Point(26, 84);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(319, 21);
            this.txtUserName.TabIndex = 3;
            // 
            // btnIngLogin
            // 
            this.btnIngLogin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(125)))), ((int)(((byte)(193)))));
            this.btnIngLogin.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnIngLogin.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnIngLogin.Location = new System.Drawing.Point(210, 183);
            this.btnIngLogin.Name = "btnIngLogin";
            this.btnIngLogin.Size = new System.Drawing.Size(135, 36);
            this.btnIngLogin.TabIndex = 1;
            this.btnIngLogin.Text = "INGRESAR";
            this.btnIngLogin.UseVisualStyleBackColor = false;
            this.btnIngLogin.Click += new System.EventHandler(this.btnIngLogin_Click);
            // 
            // btnRegister
            // 
            this.btnRegister.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(125)))), ((int)(((byte)(193)))));
            this.btnRegister.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnRegister.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnRegister.Location = new System.Drawing.Point(106, 250);
            this.btnRegister.Name = "btnRegister";
            this.btnRegister.Size = new System.Drawing.Size(135, 36);
            this.btnRegister.TabIndex = 1;
            this.btnRegister.Text = "Registrarme";
            this.btnRegister.UseVisualStyleBackColor = false;
            this.btnRegister.Click += new System.EventHandler(this.btnRegister_Click);
            // 
            // frmLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(367, 298);
            this.Controls.Add(this.txtUserName);
            this.Controls.Add(this.mtxtPass);
            this.Controls.Add(this.btnRegister);
            this.Controls.Add(this.btnIngLogin);
            this.Controls.Add(this.btnSalirLogin);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.Name = "frmLogin";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmLogin";
            this.Load += new System.EventHandler(this.frmLogin_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSalirLogin;
        private System.Windows.Forms.MaskedTextBox mtxtPass;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.Button btnIngLogin;
        private System.Windows.Forms.Button btnRegister;
    }
}