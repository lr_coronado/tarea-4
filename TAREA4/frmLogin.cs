﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using TAREA4.Entidades;

namespace TAREA4
{
    public partial class frmLogin : Form
    { 
        public frmLogin()
        {
            InitializeComponent();
        }
        private void frmLogin_Load(object sender, EventArgs e)
        {

        }

        private void btnSalirLogin_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void btnIngLogin_Click(object sender, EventArgs e)
        {
            try
            {
                // obtenemos los datos ingresados
                string UserName = txtUserName.Text.Trim();
                string Password = mtxtPass.Text.Trim();
                string Id = "";

                // utilizamos el metodo creado para validar el input 
                bool validate = mtValidarInputLogin(UserName, Password, out Id);
                if (validate == true)
                {
                    Program.UsuarioLogged = new clsUsuarios() { ID_U = Id, USERNAME_U = UserName, PASSWORD_U = Password };
                    this.Close();
                }
                else
                    MessageBox.Show("Debe ingresar su usuario y contraseña, si no está registrado debe registrarse.", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            catch (Exception ex) 
            {
                MessageBox.Show(ex.Message);
            }
        }

        private bool mtValidarInputLogin(string name, string password, out string Id)
        {
            Id = "";
            if (name != string.Empty && password != string.Empty)
            {
                // Traemos el String
                //string connectionString = Program.Connection.constructString();

                // Ejecutamos el request utilizando patrones de diseno 

                // Iniciamos un nuevo creador de request
                Program.RequestCreator.initCreator("SPC_LOGIN_GET", RequestType.SingleLoginData);

                // limpiamos los parametros
                Program.RequestCreator.ClearParams();

                // Agregamos parametros
                Program.RequestCreator.AddParameter("@NAME", name);
                Program.RequestCreator.AddParameter("@PASS", password);

                //validos la data
                var creatorResult = Program.RequestCreator.Create();

                if (creatorResult.ToString() != string.Empty)
                {
                    if (creatorResult != null)
                    {
                        string[] loginResult = creatorResult.ToString().Split('|');
                        // Validamos los resultados
                        if (name == loginResult[1] && password == loginResult[2])
                        {
                            Id = loginResult[0];
                            return true;
                        }
                        else
                            return false;
                    }
                    else
                        return false;
                }
                else
                    return false;
            }
            else
                return false;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            frmRegister register = new frmRegister();
            register.ShowDialog();

        }
    }
}
