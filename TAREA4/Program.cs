﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using TAREA4.Entidades;
using TAREA4.Common;

namespace TAREA4
{
    static class Program
    {
        // Usuario logeado 
        public static clsUsuarios UsuarioLogged;
        public static clsConnections Connection;
        public static RequestCreator RequestCreator;

        // Punto de entrada de la aplicación
        [STAThread]
        static void Main()
        {
            try
            {

                Connection = new clsConnections();
                RequestCreator = new RequestCreator();
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new frmWrapper());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
