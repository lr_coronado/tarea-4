﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TAREA4.Entidades;
using TAREA4.Common;

namespace TAREA4
{
    public partial class frmEditUser : Form
    {
        clsUsuarios Usuario;

        public frmEditUser(clsUsuarios user)
        {
            InitializeComponent();
            Usuario = user;
        }

        private void btnSalirLogin_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            try
            {
                Program.RequestCreator.initCreator("SPC_USERS_PUT", RequestType.UpdateData);
                Program.RequestCreator.ClearParams();
                Program.RequestCreator.AddParameter("@ID", this.Usuario.ID_U);
                Program.RequestCreator.AddParameter("@NOMBRE_U", boxName.Text.Trim());
                Program.RequestCreator.AddParameter("@APELLIDO_U", boxLN.Text.Trim());
                Program.RequestCreator.AddParameter("@MAIL_U", boxMail.Text.Trim());
                Program.RequestCreator.AddParameter("@TELEFONO_U", boxTelefono.Text.Trim());
                Program.RequestCreator.AddParameter("@USERNAME_U", boxUserName.Text.Trim());
                Program.RequestCreator.AddParameter("@PASSWORD_U", boxPassword.Text.Trim());
                Program.RequestCreator.AddParameter("@OPT", 1);
                Program.RequestCreator.Create();

                MessageBox.Show("Se editó el usuario correctamente.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void frmEditUser_Load(object sender, EventArgs e)
        {
            try
            {
                boxName.Text = Usuario.NOMBRE_U;
                boxLN.Text = Usuario.APELLIDO_U;
                boxMail.Text = Usuario.MAIL_U;
                boxTelefono.Text = Usuario.TELEFONO_U;
                boxUserName.Text = Usuario.USERNAME_U;
                boxPassword.Text = Usuario.PASSWORD_U;
            }
            catch (Exception ex) 
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
