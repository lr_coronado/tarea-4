﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TAREA4.Entidades;

namespace TAREA4
{
    public partial class frmMain : Form
    {
        frmEditUser frmEditUser;
        frmAddUser frmAddUser;

        public frmMain()
        {
            InitializeComponent();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            try
            {
                mtInitTable();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mtInitTable()
        {
            this.dgMain.Refresh();
            Program.RequestCreator.initCreator("SPC_USERS_GET", RequestType.DataGrid);
            Program.RequestCreator.ClearParams();
            DataTable dtUsers = (DataTable)Program.RequestCreator.Create();
            this.dgMain.DataSource = dtUsers;
        }

        private void dgMain_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                switch (e.ColumnIndex)
                {
                    case 0: mtInitModifyUser((dgMain.Rows[e.RowIndex].Cells[2].Value).ToString()); break;
                    case 1: mtInitDeleteUser((dgMain.Rows[e.RowIndex].Cells[2].Value).ToString()); break;
                    default:; break;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mtInitModifyUser(string userID)
        {
            Program.RequestCreator.initCreator("SPC_USERS_GET", RequestType.SingleData);
            Program.RequestCreator.ClearParams();
            Program.RequestCreator.AddParameter("@ID", userID);

            string x = (string)Program.RequestCreator.Create();
            string[] userData = x.Split('|');

            clsUsuarios user = new clsUsuarios()
            {
                ID_U = userData[0],
                NOMBRE_U = userData[1],
                APELLIDO_U = userData[2],
                MAIL_U = userData[3],
                TELEFONO_U = userData[4],
                USERNAME_U = userData[5],
                PASSWORD_U = userData[6],
                FECHA_INSERT = DateTime.Parse(userData[7])
            };

            frmEditUser = new frmEditUser(user);
            frmEditUser.ShowDialog();
            mtInitTable();
        }

        private void mtInitDeleteUser(string userID)
        {
            var dialogResult = MessageBox.Show("Estas Seguro que quieres eliminar este usuario?", "", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Information);
            if (dialogResult == DialogResult.Yes)
            {
                Program.RequestCreator.initCreator("SPC_USERS_PUT", RequestType.DeleteData);
                Program.RequestCreator.ClearParams();
                Program.RequestCreator.AddParameter("@ID", userID);
                Program.RequestCreator.AddParameter("@OPT", 2);
                Program.RequestCreator.Create();
                MessageBox.Show("Se eliminó correctamente el usuario.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                mtInitTable();
            }
        }

        private void btnSalirLogin_Click(object sender, EventArgs e)
        {
            Program.UsuarioLogged = null;
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmAddUser = new frmAddUser();
            frmAddUser.ShowDialog();
            mtInitTable();
        }
    }
}
