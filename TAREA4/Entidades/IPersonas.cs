﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TAREA4.Entidades
{
    public interface IPersonas
    {
        string ID_U { get; set; }
        string NOMBRE_U { get; set; }
        string APELLIDO_U { get; set; }
        string MAIL_U { get; set; }
        string TELEFONO_U { get; set; }
        DateTime FECHA_INSERT { get; set; }
        bool STATE_DELETED { get; set; }
    }
}
