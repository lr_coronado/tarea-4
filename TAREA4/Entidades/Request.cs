﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAREA4.Entidades;

namespace TAREA4.Request
{
    public abstract class Request
    {
        public RequestType RequestType;
    }

    public class DataTableGrid : Request // clase para datos en datatable
    {
        public DataTable Datos;

        public DataTableGrid(string connectionString, string CommnadRequest, List<KeyValuePair<string, object>> Parametros)
        {
            // Inicializamos nuestra connection
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open(); // Abrimos nuestra conecciont y creamos nuestro command
                using (SqlCommand command = new SqlCommand(CommnadRequest, conn) { CommandType = CommandType.StoredProcedure })
                {
                    // Revisamos y obtenemos los parametros
                    if (Parametros.Count > 0)
                    {
                        foreach (KeyValuePair<string, object> x in Parametros)
                        {
                            command.Parameters.AddWithValue(x.Key, x.Value);
                        }
                    }

                    // Ejecutamos el comando
                    SqlDataReader dataReader = command.ExecuteReader();

                    // Tomamos el resultado del Comando y lo convertimos en datatable
                    this.Datos = new DataTable();
                    this.Datos.Load(dataReader);

                    // Cerramos el reader y liberamos el command
                    dataReader.Close();
                    command.Dispose();
                }// liberamos la connection
                conn.Close();
                conn.Dispose();
            }
        }
    }

    public class SingleData : Request // Clase para datos en string
    {
        public string Datos;

        public SingleData(string connectionString, string CommnadRequest, List<KeyValuePair<string, object>> Parametros)
        {
            // Inicializamos nuestra connection
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open(); // Abrimos nuestra conecciont y creamos nuestro command
                using (SqlCommand command = new SqlCommand(CommnadRequest, conn) { CommandType = CommandType.StoredProcedure })
                {
                    // Revisamos y obtenemos los parametros
                    if (Parametros.Count > 0)
                    {
                        foreach (KeyValuePair<string, object> x in Parametros)
                        {
                            command.Parameters.AddWithValue(x.Key, x.Value);
                        }
                    }

                    // Ejecutamos el comando
                    SqlDataReader dataReader = command.ExecuteReader();

                    // Tomamos el resultado del Comando y lo convertimos en string
                    StringBuilder strBuilder = new StringBuilder();
                    while (dataReader.Read())
                    {
                        strBuilder.AppendLine($"{dataReader["ID_U"]}" +
                            $"|{dataReader["NOMBRE_U"]}" +
                            $"|{dataReader["APELLIDO_U"]}" +
                            $"|{dataReader["TELEFONO_U"]}" +
                            $"|{dataReader["MAIL_U"]}" +
                            $"|{dataReader["USERNAME_U"]}" +
                            $"|{dataReader["PASSWORD_U"]}" +
                            $"|{dataReader["FECHA_INSERT"]}");
                    }

                    // le asignamos la data a la property datos
                    this.Datos = strBuilder.ToString().Trim();

                    // Cerramos el reader y liberamos el command
                    dataReader.Close();
                    command.Dispose();
                }// liberamos la connection
                conn.Close();
                conn.Dispose();
            }
        }
    }

    public class SingleLoginData : Request // Clase para datos en string
    {
        public string Datos;

        public SingleLoginData(string connectionString, string CommnadRequest, List<KeyValuePair<string, object>> Parametros)
        {
            // Inicializamos nuestra connection
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open(); // Abrimos nuestra conecciont y creamos nuestro command
                using (SqlCommand command = new SqlCommand(CommnadRequest, conn) { CommandType = CommandType.StoredProcedure })
                {
                    // Revisamos y obtenemos los parametros
                    if (Parametros.Count > 0)
                    {
                        foreach (KeyValuePair<string, object> x in Parametros)
                        {
                            command.Parameters.AddWithValue(x.Key, x.Value);
                        }
                    }

                    // Ejecutamos el comando
                    SqlDataReader dataReader = command.ExecuteReader();

                    // Tomamos el resultado del Comando y lo convertimos en string
                    StringBuilder strBuilder = new StringBuilder();
                    while (dataReader.Read())
                    {
                        strBuilder.AppendLine($"{dataReader["ID_U"]}" +
                            $"|{dataReader["USERNAME_U"]}" +
                            $"|{dataReader["PASSWORD_U"]}");
                    }

                    // le asignamos la data a la property datos
                    this.Datos = strBuilder.ToString().Trim();

                    // Cerramos el reader y liberamos el command
                    dataReader.Close();
                    command.Dispose();
                }// liberamos la connection
                conn.Close();
                conn.Dispose();
            }
        }
    }

    public class CommandExecuteNonQuery : Request
    {
        public CommandExecuteNonQuery(string connectionString, string CommnadRequest, List<KeyValuePair<string, object>> Parametros)
        {
            // Inicializamos nuestra connection
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open(); // Abrimos nuestra conecciont y creamos nuestro command
                using (SqlCommand command = new SqlCommand(CommnadRequest, conn) { CommandType = CommandType.StoredProcedure })
                {
                    // Revisamos y obtenemos los parametros
                    if (Parametros.Count > 0)
                    {
                        foreach (KeyValuePair<string, object> x in Parametros)
                        {
                            command.Parameters.AddWithValue(x.Key, x.Value);
                        }
                    }

                    // Ejecutamos el comando
                    command.ExecuteNonQuery();

                    command.Dispose();
                }// liberamos la connection
                conn.Close();
                conn.Dispose();
            }
        }
    }
}
