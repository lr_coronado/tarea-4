﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TAREA4
{
    public enum RequestType
    {
        SingleData = 0,
        DataGrid = 1,
        JsonData = 2,
        XmlData = 3,
        UpdateData = 4,
        DeleteData = 5,
        InsertData = 6,
        SingleLoginData = 7
    }
}
