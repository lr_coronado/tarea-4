﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TAREA4.Entidades
{
    public class clsUsuarios : IPersonas
    {
        public string ID_U { get; set; }
        public string NOMBRE_U { get; set; }
        public string APELLIDO_U { get; set; }
        public string MAIL_U { get; set; }
        public string TELEFONO_U { get; set; }
        public string USERNAME_U { get; set; }
        public string PASSWORD_U { get; set; }
        public DateTime FECHA_INSERT { get; set; }
        public bool STATE_DELETED { get; set; }
    }
}