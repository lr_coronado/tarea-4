﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TAREA4
{
    public partial class frmWrapper : Form
    {
        frmMain frmMain;

        public frmWrapper()
        {
            InitializeComponent();
        }

        private void frmWrapper_Load(object sender, EventArgs e)
        {
            // Ponemos las opciones del menu deshabilitadas
            mStrip.Enabled = false;

            // Iniciamos nuestro formulario de login
            frmLogin login = new frmLogin();
            login.ShowDialog();

            // Validamos lo que se realizo en el login form
            if (Program.UsuarioLogged != null)
            {
                mStrip.Enabled = true;
                mtClearControls();
                mtInitMain();
            }
            else 
                this.Close();
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void mtInitMain() 
        {
            frmMain = new frmMain();
            frmMain.TopLevel = false;
            mainPanel.Controls.Add(frmMain);
            frmMain.Show();
            
            if (Program.UsuarioLogged == null) 
            {
                // Ponemos las opciones del menu deshabilitadas
                mStrip.Enabled = false;

                // Iniciamos nuestro formulario de login
                frmMain.Close();
                frmLogin login = new frmLogin();
                login.ShowDialog();

                // Validamos lo que se realizo en el login form
                if (Program.UsuarioLogged != null)
                {
                    mStrip.Enabled = true;
                    mtClearControls();
                    mtInitMain();
                }
                else
                    this.Close();
            }
        }

        private void mtClearControls() 
        {
            mainPanel.Controls.Clear();
        }

        private void usuariosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mtClearControls();
            mtInitMain();
        }
    }
}
